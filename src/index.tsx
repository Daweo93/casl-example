import ReactDOM from 'react-dom';
import './index.css';
import App from './App/components/Base/App';
import * as serviceWorker from './serviceWorker';
import { createContext } from './App/createContext';

const context = createContext()(App);
ReactDOM.render(context, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
