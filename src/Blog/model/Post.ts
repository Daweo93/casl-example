import * as User from '../../User/model/User';

export type Base = {
  id: string;
  title: string;
  description: string;
  image: string;
  author: User.Base;
  __type: string;
};

export type List = Array<Base>;
