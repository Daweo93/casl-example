import uuid from 'uuid/v4';
import * as Post from '../model/Post';
import { User1, User2 } from '../../User/api/User.mock';

export const POSTS: Post.List = [
  {
    author: User1,
    description:
      'Marshmallow ice cream apple pie gummies pie caramels. Sugar plum cake cake dragée dragée soufflé.',
    id: uuid(),
    image: 'https://picsum.photos/id/1025/300/200',
    title: 'Marshmallow ice cream',
    __type: 'Post'
  },
  {
    author: User2,
    description:
      'Marshmallow ice cream apple pie gummies pie caramels. Sugar plum cake cake dragée dragée soufflé.',
    id: uuid(),
    image: 'https://picsum.photos/id/1062/5092/3395',
    title: 'Marshmallow ice cream',
    __type: 'Post'
  }
];
