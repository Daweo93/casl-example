import styled from '@emotion/styled';
import {
  CardMedia as MuiCardMedia,
  CardActions as MuiCardActions,
  Card as MuiCard
} from '@material-ui/core';

export const Card = styled(MuiCard)`
  height: 100%;
`;

export const CardMedia = styled(MuiCardMedia)`
  height: 0;
  padding-top: 56.25%;
`;

export const CardActions = styled(MuiCardActions)`
  justify-content: flex-end;
`;
