import * as React from 'react';
import { FunctionComponent } from 'react';
import { Button } from '@material-ui/core';
import { CardActions as MuiCardsActions } from './PostCard.style';
import { Can } from '../../../../User/context/Ability';
import * as Post from '../../../model/Post';

export type Props = {
  post: Post.Base;
};

export const CardActions: FunctionComponent<Props> = ({ post }) => (
  <MuiCardsActions>
    <Can I="edit" of={post}>
      <Button size="small" color="primary">
        Edit
      </Button>
    </Can>
    <Can I="remove" of={post}>
      <Button size="small" color="secondary">
        Remove
      </Button>
    </Can>
  </MuiCardsActions>
);
