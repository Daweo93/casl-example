import * as React from 'react';
import { FC } from 'react';
import { Avatar, CardContent, CardHeader, Typography } from '@material-ui/core';
import * as Post from '../../../model/Post';
import * as User from '../../../../User/model/User';
import { Card, CardMedia } from './PostCard.style';
import { CardActions } from './CardActions';

export type Props = {
  post: Post.Base;
};

export const PostCard: FC<Props> = ({ post }) => {
  const { author, description, image, title } = post;
  return (
    <Card>
      <CardHeader
        avatar={<Avatar src={author.avatar}>{User.getInitials(author)}</Avatar>}
        title={title}
        subheader={User.getFullName(author)}
      />
      <CardMedia image={image} />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          {description}
        </Typography>
      </CardContent>
      <CardActions post={post} />
    </Card>
  );
};
