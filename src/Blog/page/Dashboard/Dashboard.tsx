import * as React from 'react';
import { Container } from '../../../App/components/Container/Container';
import { AddPost } from './AddPost/AddPost';
import { PostList } from './PostsList/PostsList';

export const Dashboard = () => {
  return (
    <Container>
      <AddPost />
      <PostList />
    </Container>
  );
};
