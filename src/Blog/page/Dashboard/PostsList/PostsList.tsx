import * as React from 'react';
import { POSTS } from '../../../api/Posts.mock';
import { PostCard } from '../PostCard/PostCard';
import { Grid } from '@material-ui/core';

export const PostList = () => (
  <Grid container spacing={4}>
    {POSTS.map((post) => (
      <Grid item sm={6} key={post.id}>
        <PostCard post={post} />
      </Grid>
    ))}
  </Grid>
);
