import * as React from 'react';
import styled from '@emotion/styled';
import { WithTheme } from '../../../../App/theme';
import { Button } from '@material-ui/core';
import { Can } from '../../../../User/context/Ability';

export const AddPost = () => (
  <Wrapper>
    <Can I="create" a="Post">
      <Button fullWidth variant="contained" color="primary">
        Add post
      </Button>
    </Can>
  </Wrapper>
);

const Wrapper = styled.div`
  margin: ${(props: WithTheme) => props.theme!.spacing(3, 0)};
`;
