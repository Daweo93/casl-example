import * as User from '../../User/model/User';
import { AbilityBuilder } from '@casl/ability';
import { Role } from '../../User/model/Role';

export const createBlogAbility = (user: User.Base) => {
  const { can, rules } = AbilityBuilder.extract();

  switch (user.role) {
    case Role.ADMIN:
      can(['edit', 'remove'], 'Post');
      break;
    case Role.MODERATOR:
      can(['edit'], 'Post');
      break;
    case Role.USER:
      can('create', 'Post');
      can(['edit', 'remove'], 'Post', { author: { id: user.id } });
      break;
  }

  return rules;
};
