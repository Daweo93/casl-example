import { createContext, FC } from 'react';
import { createContextualCan } from '@casl/react';
import React from 'react';
import { useUserContext } from './User';
import { createAbility } from '../../App/context/abilityContext';

export const AbilityContext = createContext({});
AbilityContext.displayName = 'AbilityContext';

export const Can = createContextualCan(AbilityContext.Consumer);
export const AbilityProvider: FC = ({ children }) => {
  const { user } = useUserContext();
  const ability = createAbility(user);
  return <AbilityContext.Provider value={ability}>{children}</AbilityContext.Provider>;
};
