import * as User from '../model/User';
import * as UserMock from '../api/User.mock';
import React, { Dispatch, FC, SetStateAction, useContext, useState } from 'react';

export type UserContext = {
  user: User.Base;
  setUser: Dispatch<SetStateAction<UserContext['user']>>;
};

const UserContext = React.createContext<UserContext>(({} as unknown) as UserContext);
UserContext.displayName = 'UserContext';

export const UserProvider: FC = ({ children }) => {
  const [user, setUser] = useState<User.Base>(UserMock.User1);

  return <UserContext.Provider value={{ user, setUser }}>{children}</UserContext.Provider>;
};

export const useUserContext = () => useContext(UserContext);
