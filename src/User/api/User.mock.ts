import * as User from '../model/User';
import { Role } from '../model/Role';

export const User1: User.Base = {
  id: 'user-1',
  name: 'John',
  surname: 'Doe',
  avatar: 'https://i.pravatar.cc/150?img=8',
  role: Role.USER
};

export const User2: User.Base = {
  id: 'user-2',
  name: 'Jane',
  surname: 'Doe',
  avatar: 'https://i.pravatar.cc/150?img=5',
  role: Role.USER
};

export const Admin: User.Base = {
  id: 'user-3',
  name: 'Admin',
  surname: 'User',
  avatar: 'https://i.pravatar.cc/150?img=7',
  role: Role.ADMIN
};

export const Moderator: User.Base = {
  id: 'user-3',
  name: 'Moderator',
  surname: 'User',
  avatar: 'https://i.pravatar.cc/150?img=7',
  role: Role.MODERATOR
};
