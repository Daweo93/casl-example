import { Role } from './Role';

export type Base = {
  name: string;
  surname: string;
  id: string;
  avatar: string;
  role: Role;
};

export const getInitials = (user: Base) => (user ? user.name[0] + '' + user.surname[0] : ' ');
export const getFullName = (user: Base) => (user ? user.name + ' ' + user.surname : ' ');
export const hasSameId = (user: Base, id: string) => user && user.id === id;
export const hasOneOfRole = (user: Base, role: Role[]) => role.find((role) => role === user.role);
