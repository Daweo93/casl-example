import { Ability, AbilityBuilder } from '@casl/ability';
import * as User from '../../User/model/User';
import { createBlogAbility } from '../../Blog/security/ability';

/**
 * Defines how to detect object's type: https://stalniy.github.io/casl/abilities/2017/07/20/define-abilities.html
 */
function subjectName(item: any) {
  if (!item || typeof item === 'string') {
    return item;
  }

  return item.__type;
}

export const createAbility = (user: User.Base) => {
  const { rules } = AbilityBuilder.extract();

  rules.push(...createBlogAbility(user));

  return new Ability(rules, { subjectName });
};
