import { createMuiTheme, Theme } from '@material-ui/core';

export type WithTheme = {
  theme?: Theme;
};

export const theme = createMuiTheme();
