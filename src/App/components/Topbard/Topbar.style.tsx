import { Toolbar as MuiToolbar } from '@material-ui/core';
import styled from '@emotion/styled';

export const Toolbar = styled(MuiToolbar)`
  display: flex;
  justify-content: space-between;
`;
