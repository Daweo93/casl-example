import * as React from 'react';
import { FC } from 'react';
import { Menu, MenuItem } from '@material-ui/core';
import { useUserContext } from '../../../../User/context/User';
import { User1, Admin, Moderator } from '../../../../User/api/User.mock';

export type Props = {
  anchorEl: Element | null;
  isMenuOpen: boolean;
  handleMenuClose: () => void;
};

export const UserMenu: FC<Props> = ({ anchorEl, isMenuOpen, handleMenuClose }) => {
  const { setUser } = useUserContext();
  return (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id="primary-search-account-menu"
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem
        onClick={() => {
          setUser(User1);
          handleMenuClose();
        }}
      >
        User
      </MenuItem>
      <MenuItem
        onClick={() => {
          setUser(Moderator);
          handleMenuClose();
        }}
      >
        Moderator
      </MenuItem>
      <MenuItem
        onClick={() => {
          setUser(Admin);
          handleMenuClose();
        }}
      >
        Admin
      </MenuItem>
    </Menu>
  );
};
