import * as React from 'react';
import { AppBar, Avatar, Typography } from '@material-ui/core';
import { UserMenu } from './UserMenu/UserMenu';
import { Toolbar } from './Topbar.style';
import { useUserContext } from '../../../User/context/User';
import * as User from '../../../User/model/User';

export const Topbar = () => {
  const userContext = useUserContext();
  const [anchorEl, setAnchorEl] = React.useState<Element | null>(null);
  const isMenuOpen = Boolean(anchorEl);

  const handleProfileMenuOpen = (event: any) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6">CoolBlog</Typography>
          <Avatar onClick={handleProfileMenuOpen}>
            <Typography>{User.getInitials(userContext.user)}</Typography>
          </Avatar>
        </Toolbar>
      </AppBar>
      <UserMenu anchorEl={anchorEl} handleMenuClose={handleMenuClose} isMenuOpen={isMenuOpen} />
    </>
  );
};
