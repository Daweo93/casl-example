import React from 'react';
import { Topbar } from '../Topbard/Topbar';
import { Dashboard } from '../../../Blog/page/Dashboard/Dashboard';

const App: React.FC = () => {
  return (
    <>
      <Topbar />
      <Dashboard />
    </>
  );
};

export default App;
