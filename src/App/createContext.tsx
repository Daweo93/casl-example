import React, { ComponentType, ReactElement } from 'react';
import { CssBaseline } from '@material-ui/core';
import { theme } from './theme';
import { ThemeProvider } from 'emotion-theming';
import { UserProvider } from '../User/context/User';
import { AbilityProvider } from '../User/context/Ability';

export function createContext() {
  return (Component: ComponentType<any>): ReactElement<any> => (
    <>
      <CssBaseline />
      <ThemeProvider theme={theme}>
        <UserProvider>
          <AbilityProvider>
            <Component />
          </AbilityProvider>
        </UserProvider>
      </ThemeProvider>
    </>
  );
}
